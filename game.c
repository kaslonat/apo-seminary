#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "game.h"
#include "display_utils.h"
#include "led_utils.h"
#include "kbhit.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

static bool quit_game;

static void check_collision(struct Board *board1, struct Ball *ball);
static void check_goal(struct Board *board1, struct Board *board2,
        struct Ball *ball, struct Hearts *Hearts, unsigned short *fb,
        unsigned char* mem_base, unsigned char *parlcd_mem_base);
static void check_game_over(struct Hearts *hearts, unsigned short *fb,
        unsigned char* mem_base, unsigned char *parlcd_mem_base);

static void init_new_round(struct Board *board1, struct Board *board2,
        struct Ball *ball, short win_player, struct Hearts *hearts,
        unsigned short *fb, unsigned char* mem_base, unsigned char *parlcd_mem_base);

static void game_input_auto(struct Board *board1, struct Board *board2, struct Ball *ball);
static void game_input_single(struct Board *board1, struct Board *board2, struct Ball *ball);
static void game_input_multi(struct Board *board1, struct Board *board2, struct Ball *ball);

static void game_update(struct Board *board1, struct Board *board2, struct Ball *ball,
        struct Hearts *hearts, unsigned short *fb, unsigned char* mem_base,
        unsigned char *parlcd_mem_base);


void game_run(mode m, unsigned char *parlcd_mem_base)
{
    unsigned short *fb = (unsigned short *)calloc(MAX_SIZE, sizeof(unsigned short));
    struct Board board1 = board_init(LEFT);
    struct Board board2 = board_init(RIGHT);
    struct Ball ball = ball_init(RIGHT);
    struct Hearts hearts;
    hearts.player1 = MAX_HP;
    hearts.player2 = MAX_HP;
    quit_game = false;

    unsigned char *mem_base = mem_base_init();
    clear_led_line(mem_base);
    clear_led(mem_base);

    void (*input_fnc)(struct Board *board1, struct Board *board2, struct Ball *ball);
    switch (m) {
        case MULTIPLAYER:
            input_fnc = game_input_multi;
            break;
        case SINGLEPLAYER:
            input_fnc = game_input_single;
            break;
        case AUTOPLAYER:
            input_fnc = game_input_auto;
            break;
        default:
            input_fnc = game_input_auto;
            break;
    }

    //check input
    while (!quit_game) {
        game_update(&board1, &board2, &ball, &hearts, fb, mem_base, parlcd_mem_base);
        input_fnc(&board1, &board2, &ball);
        game_draw(&board1, &board2, &ball, &hearts, fb, parlcd_mem_base);
    }
    // free alocated memory
    free(fb);
}

void game_input_auto(struct Board *board1, struct Board *board2, struct Ball *ball)
{
    board_auto_move(board1, ball);
    board_auto_move(board2, ball);
    change_mode(1);
    if (kbhit()) {
        char ch = getchar();
        if (ch == 'q')
            quit_game = true;
    }
    change_mode(0);
}

void game_input_single(struct Board *board1, struct Board *board2, struct Ball *ball)
{
    char ch;
    change_mode(1);
    if (kbhit()) {
        ch = getchar();
        switch (ch) {
            case 'w':
                board_move_up(board1);
                break;
            case 's':
                board_move_down(board1);
                break;
            case 'q':
                quit_game = true;
                break;
        }
    }
    change_mode(0);
    board_auto_move(board2, ball);
}

void game_input_multi(struct Board *board1, struct Board *board2, struct Ball *ball)
{
    char ch;
    change_mode(1);
    if (kbhit()){
        ch = getchar();
        switch (ch){
            case 'w':
                board_move_up(board1);
                break;
            case 's':
                board_move_down(board1);
                break;
            case 'i':
                board_move_up(board2);
                break;
            case 'k':
                board_move_down(board2);
                break;
            case 'q':
                quit_game = true;
                break;
        }
    }
    change_mode(0);
}

void game_update (struct Board *board1, struct Board *board2, struct Ball *ball, 
        struct Hearts *hearts, unsigned short *fb, unsigned char* mem_base,
        unsigned char *parlcd_mem_base)
{
    ball_move(ball);
    check_collision(board1, ball);
    check_collision(board2, ball);
    ball_check_bounce(ball);
    check_goal(board1, board2, ball, hearts, fb, mem_base, parlcd_mem_base);
}

static void check_collision(struct Board *board1, struct Ball *ball)
{
    if (board1->side == LEFT) {
        // Check x 
        if (board1->posX <= ball->posX && board1->posX+BOARD_WIDTH >= ball->posX) {
            // Check y
            if (board1->posY <= ball->posY+BALL_SIZE && 
                    board1->posY+BOARD_HEIGHT >= ball->posY) {
                ball->velX *= -1;
                ball->velX += ball->velX > 0 ? 0.03 : -0.3; 
                ball->velY = (-(board1->posY + BOARD_HEIGHT/2) + (ball->posY + BALL_SIZE/2))
                                / (BOARD_HEIGHT/2) *2;
                ball->posX = board1->posX+BOARD_WIDTH;
            }
        }
    } else {
        if (board1->posX <= ball->posX+BALL_SIZE &&
                board1->posX+BOARD_WIDTH >= ball->posX+BALL_SIZE) {
            if (board1->posY <= ball->posY && board1->posY+BOARD_HEIGHT >= ball->posY) {
                ball->velX *= -1;
                ball->velX += ball->velX > 0 ? 0.03 : -0.3; 
                ball->velY = (-(board1->posY + BOARD_HEIGHT/2) + (ball->posY + BALL_SIZE/2))
                                / (BOARD_HEIGHT/2) *2;
                ball->posX = board1->posX-BALL_SIZE;
            }
        }
    }
}

static void check_goal(struct Board *board1, struct Board *board2, struct Ball *ball, 
        struct Hearts *hearts, unsigned short *fb, unsigned char* mem_base,
        unsigned char *parlcd_mem_base)
{
    if (ball->posX+BALL_SIZE <= MIN_WIDTH) {
        hearts->player1--;
        init_new_round(board1, board2, ball, RIGHT, hearts, fb, mem_base, parlcd_mem_base);
    }
    else if (ball->posX-BALL_SIZE >= MAX_WIDTH) {
        hearts->player2--;
        init_new_round(board1, board2, ball, LEFT, hearts, fb, mem_base, parlcd_mem_base);
    }
}

static void init_new_round(struct Board *board1, struct Board *board2,
        struct Ball *ball, short win_player, struct Hearts *hearts,
        unsigned short *fb, unsigned char* mem_base, unsigned char *parlcd_mem_base)
{
    *ball = ball_init(win_player);
    *board1 = board_init(LEFT);
    *board2 = board_init(RIGHT);
    check_game_over(hearts, fb, mem_base, parlcd_mem_base);
    if (!quit_game){
        life_lose_led_line(mem_base);
        life_lose_led(mem_base, win_player);
        clear_led_line(mem_base);
    }
}

static void check_game_over(struct Hearts *hearts, unsigned short *fb,
        unsigned char* mem_base, unsigned char *parlcd_mem_base) 
{
    if (hearts->player1 > 0 && hearts->player2 > 0)
        return;

    signed char win_side;
    if (hearts->player1 == 0)
        win_side = RIGHT;
    else
        win_side = LEFT;
    
    game_draw_background(fb);
    game_draw_hearts(hearts, fb);
    game_draw_end(win_side, fb);
    for (int ptr = 0; ptr < MAX_SIZE ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
    end_game_led(mem_base, win_side);

    bool led_mode = false;
    bool quit = false;
    while (!quit) {
        change_mode(1);
        if (kbhit()) {
            char ch = getchar();
            if (ch == 'q') {
                quit_game = true;
                quit = true;
                clear_led(mem_base);
                clear_led_line(mem_base);
                return;
            }
        }
        change_mode(0);
        end_game_led_line(mem_base, led_mode);
        led_mode = led_mode ? false : true;
    }
}
