#include "ball.h"

#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#include "display_utils.h"

struct Ball ball_init(signed char dir)
{
    srand((unsigned int)time(NULL));

    struct Ball b;
    b.velX = (float)dir*2;
    b.velY = ((float)rand()/(float)(RAND_MAX))*4 - 2;
    b.posX = MAX_WIDTH/2 - BALL_SIZE/2;
    b.posY = MAX_HEIGHT/2 - BALL_SIZE/2;
    return b;
}

void ball_move(struct Ball *b)
{
    b->posX += b->velX;
    b->posY += b->velY;
}

void ball_check_bounce(struct Ball *b)
{
    if (b->posY < MIN_HEIGHT + BACKGROUND_WIDTH){
        b->posY = MIN_HEIGHT + BACKGROUND_WIDTH;
        b->velY *= -1;
    } else if (b->posY + BALL_SIZE > MAX_HEIGHT - BACKGROUND_WIDTH){
        b->posY = MAX_HEIGHT - BALL_SIZE - BACKGROUND_WIDTH;
        b->velY *= -1;
    }
}

void ball_draw(struct Ball *b, unsigned short *fb)
{
    for (int i = 0; i < BALL_SIZE; i++){
        for (int j = 0; j < BALL_SIZE; j++){
            draw_pixel(b->posX + j, b->posY + i, WHITE, fb);
            draw_pixel(b->posX + j, b->posY + i, WHITE, fb);
        }
    }
}
