#include <stdio.h>
#include <stdlib.h>

#include "display_utils.h"
#include "font_types.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#define MAX_WIDTH 480
#define MIN_WIDTH 0
#define MAX_HEIGHT 320
#define MIN_HEIGHT 0
#define MAX_SIZE 153600

unsigned char* mem_base_init()
{
    unsigned char *mem_base;
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (mem_base == NULL)
      exit(1);
    return mem_base;
}

unsigned char* display_init()
{
    unsigned char *mem_base;
    unsigned char *parlcd_mem_base;

    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (mem_base == NULL)
    exit(1);

    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL)
        exit(1);

    parlcd_hx8357_init(parlcd_mem_base);
    parlcd_write_cmd(parlcd_mem_base, 0x2c);

    return parlcd_mem_base;
}

void clear_buffer(unsigned short *fb)
{
    for (int i = 0; i < MAX_SIZE ; i++) {
        fb[i] = 0;
    }
}

void clear_display(unsigned char *parlcd_mem_base)
{
    for (int i = 0; i < MAX_SIZE ; i++) {
        parlcd_write_data(parlcd_mem_base, 0);
    }
}

void draw_pixel(int x, int y, unsigned short color, unsigned short *fb)
{
  if (x>=MIN_WIDTH && x<MAX_WIDTH && y>=MIN_HEIGHT && y<MAX_HEIGHT) {
    fb[x+480*y] = color;
  }
}

void draw_pixel_big(int x, int y, int scale, unsigned short color, unsigned short *fb)
{
  int i,j;
  for (i=0; i<scale; i++) {
    for (j=0; j<scale; j++) {
      draw_pixel(x+i, y+j, color, fb);
    }
  } 
}



int char_width(int ch, font_descriptor_t *fdes)
{
  int width;
  if (!fdes->width) {
    width = fdes->maxwidth;
  } else {
    width = fdes->width[ch-fdes->firstchar];
  }
  return width;
}

void draw_char(int x, int y, char ch, int scale, unsigned short color,
    unsigned short *fb, font_descriptor_t *fdes)
{
  int w = char_width(ch, fdes);
  const font_bits_t *ptr;
  if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
    if (fdes->offset) {
      ptr = &fdes->bits[fdes->offset[ch-fdes->firstchar]];
    } else {
      int bw = (fdes->maxwidth+15)/16;
      ptr = &fdes->bits[(ch-fdes->firstchar)*bw*fdes->height];
    }
    int i, j;
    for (i=0; i<fdes->height; i++) {
      font_bits_t val = *ptr;
      for (j=0; j<w; j++) {
        if ((val&0x8000)!=0) {
          draw_pixel_big(x+scale*j, y+scale*i, scale, color, fb);
        }
        val<<=1;
      }
      ptr++;
    }
  }
}

void draw_word(int x, int y, char* word, int length, int scale,     
             unsigned short color, unsigned short *fb, font_descriptor_t *fdes)
{
  int space = fdes == &font_winFreeSystem14x16 ? 60 : 20;
  for (int i = 0; i < length; i++){
    draw_char(x + (i - 1)*space, y, word[i], scale, 0xFFFF, fb, fdes);
  }
}
