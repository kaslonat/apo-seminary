#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#include "menu.h"
#include "game.h"
#include "display_utils.h"
#include "kbhit.h"
#include "game_properties.h"

#define REC_WIDTH 340
#define REC_HEIGHT 40
#define REC_SCALE 4

#define SCALE_BIG 5
#define SCALE_SMALL 2

static void menu_draw(unsigned char *parlcd_mem_base);
static void draw_buttons(unsigned short *fb, font_descriptor_t *fdes);
static void draw_rectangle(unsigned short *fb, int x, int y);

void menu_run()
{
    // set LCD adress
    unsigned char *parlcd_mem_base = display_init();

    char ch;
    mode m;
    bool quit = false;
    bool set_mode = false;
    while (!quit){
        menu_draw(parlcd_mem_base);

        // check if key was pressed and start game
        while (!quit){
            change_mode(1);
            if (kbhit())
            {
                ch = getchar();
                switch (ch){
                    case 'q':
                        quit = true;
                        // TODO free display buffer
                        break;
                    case 'm':
                        set_mode = true;
                        m = MULTIPLAYER;
                        break;
                    case 's':
                        set_mode = true;
                        m = SINGLEPLAYER;
                        break;
                    case 'a':
                        set_mode = true;
                        m = AUTOPLAYER;
                        break;
                    default:
                        printf("Invalid key!\n");
                }

                if (set_mode){
                    change_mode(0);
		            set_mode = false;
                    game_run(m, parlcd_mem_base);
                    menu_draw(parlcd_mem_base);
                }
            }
            change_mode(0);
        }
    }
    
    change_mode(0);
    clear_display(parlcd_mem_base);
}

static void menu_draw(unsigned char *parlcd_mem_base)
{
    clear_display(parlcd_mem_base);

    // create buffer
    unsigned short *fb;
    fb  = (unsigned short *)calloc(MAX_SIZE, sizeof(unsigned short));
    font_descriptor_t *fdes_big = &font_winFreeSystem14x16;
    font_descriptor_t *fdes_small = &font_rom8x16;

    draw_word(160, 10, "PONG", 4, SCALE_BIG, WHITE, fb, fdes_big);
    draw_buttons(fb, fdes_small);
    
    //push to display
    for (int ptr = 0; ptr < MAX_SIZE ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
    
    free(fb);
}


static void draw_buttons(unsigned short *fb, font_descriptor_t *fdes){
    draw_word(100, 100, "Multiplayer [m]", 15, SCALE_SMALL, WHITE, fb, fdes);
    draw_word(90, 170, "Singleplayer [s]", 16, SCALE_SMALL, WHITE, fb, fdes);
    draw_word(110, 240, "Autoplayer [a]", 14, SCALE_SMALL, WHITE, fb, fdes);
    draw_word(340, 280, "Quit [q]", 8, SCALE_SMALL, WHITE, fb, fdes);

    draw_rectangle(fb, 60, 90);
    draw_rectangle(fb, 60, 160);
    draw_rectangle(fb, 60, 230);
}

static void draw_rectangle(unsigned short *fb, int x, int y){
    if (x<MIN_WIDTH || x>=MAX_WIDTH || y<MIN_HEIGHT || y>=MAX_HEIGHT)
        return;
    
    for (int i = 0; i < REC_WIDTH; i += REC_SCALE){
        draw_pixel_big(x + i, y, REC_SCALE, WHITE, fb);
        draw_pixel_big(x + i, y + REC_HEIGHT, REC_SCALE, WHITE, fb);
    }

    for (int j = 0; j < REC_HEIGHT; j += REC_SCALE){
        draw_pixel_big(x, y + j, REC_SCALE, WHITE, fb);
        draw_pixel_big(x + REC_WIDTH, y + j, REC_SCALE, WHITE, fb);
    }
}
