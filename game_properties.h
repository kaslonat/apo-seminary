#ifndef __GAME_PROPERTIES_H__
#define __GAME_PROPERTIES_H__

#define MAX_SIZE 153600
#define MAX_WIDTH 480
#define MIN_WIDTH 0
#define MAX_HEIGHT 320
#define MIN_HEIGHT 0

#define BACKGROUND_WIDTH 15

#define BOARD_WIDTH 15
#define BOARD_HEIGHT 40
#define BOARD_SPEED 5

#define BALL_SIZE 8 

#define RED 0xF800
#define BLUE 0x001F
#define WHITE 0xDDDD

#define RED_LED 0x00FF0000
#define BLUE_LED 0x000000FF

#define INDENT 20

#define LEFT -1
#define RIGHT 1

#define MAX_HP 3

#endif  // __GAME_PROPERTIES_H__