#ifndef __MENU_H__
#define __MENU_H__

/* 
 * Function:  menu_run
 * --------------------
 * Set game menu and wait for mode options. 
 */
void menu_run();


#endif  // __MENU_H__