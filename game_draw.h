#ifndef __GAME_DRAW_H__
#define __GAME_DRAW_H__

#include "board.h"
#include "ball.h"
#include "game_properties.h"

struct Hearts{
    short player1;
    short player2;
};

/* 
 * Function:  game_draw
 * --------------------
 * Run all other draw functins neccessary for game.
 *
 * board1: pointer to first board struct
 * board2: pointer to second board struct
 * ball: pointer to ball struct
 * hearts: pointer to heart struct
 * fb: data buffer given to display
 * parlcd_mem_base: pointer to display memory
 */
void game_draw(struct Board *board1, struct Board *board2,
        struct Ball *ball, struct Hearts *hearts, unsigned short *fb,
        unsigned char *parlcd_mem_base);

/* 
 * Function:  game_draw_background
 * --------------------
 * Draw upper and bottom border line, colored goal area.
 *
 * fb: data buffer given to display
 */
void game_draw_background(unsigned short *fb);

/* 
 * Function:  game_draw_hearts
 * --------------------
 * Draw hearts for both players over top border line.
 *
 * hearts: pointer to heart struct
 * fb: data buffer given to display
 */
void game_draw_hearts(struct Hearts *hearts, unsigned short *fb);

/* 
 * Function:  game_draw_end
 * --------------------
 * Draw end screen with name of winning player.
 *
 * win_side: -1 for left player, 1 for right player
 * fb: data buffer given to display
 */
void game_draw_end(signed char win_side, unsigned short *fb);


#endif  // __GAME_DRAW_H__