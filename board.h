#ifndef __BAORD_H__
#define __BAORD_H__

#include <stdbool.h>
#include "game_properties.h"
#include "ball.h"


struct Board{
    float posX;
    float posY;
    signed char side;
};

/* 
 * Function:  board_init
 * -----------------------
 * Initialize board and set position to middle.
 *
 * side: -1 for left side, 1 for right side
 */
struct Board board_init(signed char side);

/* 
 * Function:  board_move_up
 * -----------------------
 * Update board position for moving up.
 *
 * b: pointer to board to be moved
 */
void board_move_up(struct Board *b);

/* 
 * Function:  board_move_down
 * -----------------------
 * Update board position for moving down.
 *
 * b: pointer to board to be moved
 */
void board_move_down(struct Board *b);

/* 
 * Function:  board_auto_move
 * -----------------------
 * Simple AI.
 *
 * b: pointer to board to be moved
 * ball: board is moving according to ball position
 */
void board_auto_move(struct Board *b, struct Ball *ball);

/* 
 * Function:  board_draw
 * -----------------------
 * Draw board on hit position
 *
 * b1: pointer to first board to be drawn
 * b2: pointer to second board to be drawn
 * fb: data buffer given to display
 */
void board_draw(struct Board *b1, struct Board *b2, unsigned short *fb);

#endif  // __BOARD_H__