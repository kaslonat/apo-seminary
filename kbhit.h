#ifndef __KBHIT_H__
#define __KBHIT_H__

/*
 * Implementation from Thantos, Cprogramming blok
 * https://cboard.cprogramming.com/linux-programming/51531-faq-cached-input-mygetch.html?highlight=kbhit
 */


/* 
 * Function:  change_mode
 * --------------------
 * Set terminal mode, whether you can write on or you can not and 
 * program can read. 
 * Don't forget to ALWAYS set back on 0.
 *
 * dir: set on 1, if you want to check if is there any waiting char.
 *      Blockc terminal.
 *      set on 0, if you want to write.
 */
void change_mode(int dir);


/* 
 * Function:  kbhit
 * --------------------
 * returns: zero if no key was pressed
 *          non zero  if there is a char waiting in stdin
 */
int kbhit(void);

#endif  // __KBHIT_H__