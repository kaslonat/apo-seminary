/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  pong.c      - main file

  (C) Copyright 2021 by Natalie Kaslova and Antonin Plevac 
  This program is free software: you can redistribute it and/or modify it

 *******************************************************************/

#include "stdio.h"

#include "serialize_lock.h"
#include "menu.h"

int main(int argc, char *argv[])
{

  /* Try to acquire lock the first */
  if (serialize_lock(1) <= 0) {
    printf("System is occupied\n");

    if (1) {
      printf("Waitting\n");
      /* Wait till application holding lock releases it or exits */
      serialize_lock(0);
    }
  }

  menu_run();

  return 0;
}
