#include <stdbool.h>
#include "board.h"
#include "display_utils.h"

struct Board board_init(signed char side)
{
    struct Board b;
    b.side = side;
    b.posX = side == LEFT ? INDENT + BACKGROUND_WIDTH :
            MAX_WIDTH - INDENT - BOARD_WIDTH - BACKGROUND_WIDTH;
    b.posY = MAX_HEIGHT/2 - BOARD_HEIGHT/2;
    
    return b;
}   

void board_move_up(struct Board *b)
{
    b->posY -= BOARD_SPEED;
    b->posY = b->posY < MIN_HEIGHT + BACKGROUND_WIDTH ?
            MIN_HEIGHT + BACKGROUND_WIDTH : b->posY;
}

void board_move_down(struct Board *b)
{
    b->posY += BOARD_SPEED;
    b->posY = b->posY > MAX_HEIGHT - BOARD_HEIGHT - BACKGROUND_WIDTH ? 
            MAX_HEIGHT - BOARD_HEIGHT - BACKGROUND_WIDTH : b->posY;
}

void board_auto_move(struct Board *board, struct Ball *ball)
{
    if ((board->side == LEFT && ball->velX < 0) || 
            (board->side == RIGHT && ball->velX > 0)) {
        if (board->posY+BOARD_HEIGHT/2 - 15 > ball->posY+BALL_SIZE/2)
            board_move_up(board);
        else if (board->posY + BOARD_HEIGHT/2 + 15 < ball->posY + BALL_SIZE/2)
            board_move_down(board);
    } else {
        if (board->posY > MAX_HEIGHT/2)
            board_move_up(board);
        else if (board->posY < MAX_HEIGHT/2 - BOARD_HEIGHT/2)
            board_move_down(board);
    }
} 

void board_draw(struct Board *b1, struct Board *b2, unsigned short *fb)
{
    for (int i = 0; i < BOARD_HEIGHT; i++){
        for (int j = 0; j < BOARD_WIDTH; j++){
            draw_pixel(b1->posX + j, b1->posY + i, BLUE, fb);
            draw_pixel(b2->posX + j, b2->posY + i, RED, fb);
        }
    }
}
