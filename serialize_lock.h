#ifndef SERIALIZE_LOCK_H
#define SERIALIZE_LOCK_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* 
 * Function:  serialize_lock
 * --------------------
 * Lock MZ_APO board so other can not use it.
 */
int serialize_lock(int no_wait);

/* 
 * Function:  serialize_lock
 * --------------------
 * Unlock MZ_APO board so other can use it.
 */
void serialize_unlock(void);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif  /*SERIALIZE_LOCK_H*/