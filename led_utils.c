#include <stdint.h>
#include <stdbool.h>

#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "time.h"
#include "game_properties.h"


void end_game_led_line(unsigned char *mem_base, bool mode)
{
    volatile uint32_t *val_line = (volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o);
    struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 500 * 1000 * 1000};
    *val_line = 0x55555555;
    *val_line = mode ? *val_line<<1 : *val_line;
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
}

void life_lose_led_line(unsigned char *mem_base)
{
    volatile uint32_t *val_line = (volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o);
    *val_line = 0xFFFFFFFF;
}

void clear_led_line(unsigned char *mem_base)
{
    volatile uint32_t *val_line = (volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o);
    *val_line = 0;
}

void end_game_led(unsigned char *mem_base, signed char player)
{
    uint32_t color = player == LEFT ? BLUE_LED : RED_LED;
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = color;
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = color;
}

void life_lose_led(unsigned char *mem_base, short player)
{
    uint32_t color = player == LEFT ? BLUE_LED : RED_LED;
    volatile uint32_t *led_adress = (player == LEFT) ? 
        (volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) : 
        (volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o);
    
    struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 50 * 1000 * 1000};
    for (int i = 0; i < 20; i++){
        *led_adress = i % 2 == 0 ? color : 0;
        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    }

}

void clear_led(unsigned char *mem_base)
{
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0;
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0;
}
