#include "game_draw.h"
#include "display_utils.h"
#include "mzapo_parlcd.h"
#include "mzapo_regs.h"

#define HEART 0x03
#define HEART_SCALE 1
#define FONT_WIDTH 14
#define SCALE_BIG 5
#define SCALE_SMALL 2

void game_draw(struct Board *board1, struct Board *board2,
        struct Ball *ball, struct Hearts *hearts, unsigned short *fb,
        unsigned char *parlcd_mem_base)
{
    clear_buffer(fb);
    game_draw_background(fb);
    game_draw_hearts(hearts, fb);
    board_draw(board1, board2, fb);
    ball_draw(ball, fb);
    for (int ptr = 0; ptr < MAX_SIZE ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}

void game_draw_background(unsigned short *fb)
{
    for (int i = 0; i < MAX_WIDTH; ++i) {
        for (int j = 0; j < BACKGROUND_WIDTH; ++j) {
            draw_pixel(i, j, WHITE, fb);
            draw_pixel(i, MAX_HEIGHT-j, WHITE, fb);
        }
    }
    for (int i = 0; i < BACKGROUND_WIDTH; ++i) {
        for (int j = BACKGROUND_WIDTH+10; j < MAX_HEIGHT-BACKGROUND_WIDTH-10; ++j) {
            draw_pixel(i, j, BLUE, fb);
            draw_pixel(MAX_WIDTH-i, j, RED, fb);
        }
    }
}

void game_draw_hearts(struct Hearts *hearts, unsigned short *fb)
{
    font_descriptor_t *fdes_small = &font_rom8x16;
    for (int i = 0; i < hearts->player1; ++i) {
        draw_char(MIN_WIDTH + i*FONT_WIDTH + 5, MIN_HEIGHT, HEART, HEART_SCALE,
                0, fb, fdes_small);
    }
    for (int i = 1; i <= hearts->player2; ++i) {
        draw_char(MAX_WIDTH - i*FONT_WIDTH, MIN_HEIGHT, HEART, HEART_SCALE,
                0, fb, fdes_small);
    }
}

void game_draw_end(signed char win_side, unsigned short *fb)
{
    font_descriptor_t *fdes_small = &font_rom8x16;
    char text_player[14] = "P L A Y E R  X";
    char text_win[6] = "W O N";
    if (win_side == LEFT) 
        text_player[13] = '1'; 
    else 
        text_player[13] = '2';
    draw_word(120, 50, text_player, 14, SCALE_BIG, WHITE, fb, fdes_small);
    draw_word(200, 120, text_win, 6, SCALE_BIG, WHITE, fb, fdes_small); 
    draw_word(70, 200, "press [q] to go back", 21, SCALE_SMALL, WHITE, fb, fdes_small);
    draw_word(150, 240, "to the menu", 12, SCALE_SMALL, WHITE, fb, fdes_small);
}
