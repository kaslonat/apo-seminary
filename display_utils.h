#ifndef __DISPLAY_UTILS_H__
#define __DISPLAY_UTILS_H__

#include "font_types.h"

/* 
 * Function:  mem_base_init
 * --------------------
 * Initialize adress to periferies, check if run trought alright.
 *
 * returns: display adress
 */
unsigned char* mem_base_init();

/* 
 * Function:  display_init
 * --------------------
 * Initialize display, check if run trought alright.
 *
 * returns: display adress
 */
unsigned char* display_init();

/* 
 * Function:  clear_buffer
 * --------------------
 * Clear whole buffer = black colour.
 *
 * fb: data buffer given to display
 */
void clear_buffer(unsigned short *fb);

/* 
 * Function:  clear_display
 * --------------------
 * Set whole display on black.
 *
 * prlcd_mem_base: display adress
 */
void clear_display(unsigned char *parlcd_mem_base);

/* 
 * Function:  draw_pixel
 * --------------------
 * Draw single pixel on set position, with set color.
 *
 * x: x position of display where should be pixel drawn
 * y: y position of display where should be pixel drawn
 * color: color of drawn pixel
 * fb: data buffer given to display
 */
void draw_pixel(int x, int y, unsigned short color, unsigned short *fb);

/* 
 * Function:  draw_pixel_big
 * --------------------
 * Draw big pixel (square with size of 'scale') on set position, with set color.
 *
 * x: x position of display where should be big pixel drawn
 * y: y position of display where should be big pixel drawn
 * scale: size of big pixel
 * color: color of drawn big pixel
 * fb: data buffer given to display
 */
void draw_pixel_big(int x, int y, int scale, unsigned short color, unsigned short *fb);

/* 
 * Function:  char_width
 * --------------------
 * Find out char width.
 *
 * ch: asked character
 * fder: adress of font descpritor
 *       use font_winFreeSystem14x16 or &font_rom8x16
 */
int char_width(int ch, font_descriptor_t *fdes);

// font_descriptor_t *fdes = &font_winFreeSystem14x16;
//                        or &font_rom8x16;

/* 
 * Function:  draw_pixel_big
 * --------------------
 * Draw big pixel (square with size of 'scale') on set position, with set color.
 *
 * x: x position of display where should be char drawn
 * y: y position of display where should be char drawn
 * scale: size of char
 * color: color of drawn char
 * fb: data buffer given to display
 */
void draw_char(int x, int y, char ch, int scale, unsigned short color, unsigned short *fb, font_descriptor_t *fdes);

void draw_word(int x, int y, char* word, int length, int scale,     //
             unsigned short color, unsigned short *fb, font_descriptor_t *fdes);

#endif  // __DISPLAY_UTILS_H__