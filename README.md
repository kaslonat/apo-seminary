# Manuál - Pong

Program Pong je jednoduchá hra pro 0 až 2 hráče. Ve hře proti sobě hrají vždy 2 hráči (člověk vs člověk,
člověk vs počítač, počítač vs počítač), kteří se snaží nedostat gól a zároveň dostat míček do protihráčovi
brány. Na začátku hry mají oba hráči stejně životů. Na začátku každého kola začíná míček na středu
a letí náhodným směrem na toho, kdo vyhrál minulé kolo. Čím déle kolo trvá tím rychleji se míček pohybuje
a zvyšuje tím obtížnost hry. V případě, že hráč dostane gól, odebere se mu jeden život, LEDky zablikají
v barvě skórujícího hráče a začne nové kolo. Hra končí v momentě, kdy jeden z hráčů přijde o poslední život.
Při výhře jednoho z hráčů se také rozsvítí LEDky v barvě vyhrávajícího hráče a rozbliká se LEDpásek.
Také se zobrazí jméno vyhrávajícího hráče a je možné se vrátit zpět do menu.

Po zapnutí hry se jako první zobrazí menu. V menu si lze pomocí těchto kláves vybrat právě jeden z herní módů:
- 'a' Autoplayer (počítač vs počítač)
- 's' Singleplayer (člověk vs počítač)
- 'm' Multiplayer (člověk vs člověk)
- 'q' pro vypnutí hry

Po vybrání hracího módu se zobrazí hra s těmito komponenty:
- horní i spodní okraje tvoří dvě bílé lišty, od kterých se míček odráží
- v levém a pravém horním rohu jsou v bílých lištách zobrazeny srdíčka hráčů hrající na této straně
- levý okraj tvoří modrá lišta, která představuje bránu modrého hráče
- pravý okraj tvoří červená lišta, která představuje bránu červeného hráče
- kousek před bránou každého hráče je jeho deska se stejnou barvou jako hráčova brána
- míček je bílé barvy a na začátku kola je vždy na středu hracího pole
- zbytek hracího pole je černý

Hru lze ovládat následujícími tlačítky:
- 'w' pro posunutí desky modrého hráče nahoru
- 's' pro posunutí desky modrého hráče dolů
- 'i' pro posunutí desky červeného hráče nahoru
- 'k' pro posunutí desky červeného hráče dolů
- 'q' pro odchod zpět do menu


## UML diagram

![ups something went wrong](uml_diagram.png "UML diagram")


## Výčet funkcí
- pong.c
    - main
        - Main funkce na spuštění programu.

- menu.c
    - menu_run
        - Nastaví a vykreslí menu na obrazovku.

- game.c
    - game_run
        - Spustí hru podle zadaného modu.

- game_draw.c
    - game_draw
        - Spustí ostatní potřebné funkce na vykreslení hry.
    - game_draw_background
        - Vykreslí ohraničení hrací plochy.
    - game_draw_hearts
        - Vykreslí životy obou hráčů přes horní okrajovou linku.
    - game_draw_end
        - Vypíše vyhrávajícího hráče a instrukce na návrat do menu.

- ball.c
    - ball_init
        - Inicializuje míček a nastaví pozici na střed hracího pole.
    - ball_move
        - Aktualizuje pozici míčku.
    - ball_check_bounce
        - Zkontroluje, jestli míček nenarazil na horní nebo spodní okraj hracího pole,
          v případě že narazil, zkounstruuje odraz.
    - ball_draw
        - Vykreslí míček na jeho pozicích.

- board.c
    - board_init
        - Inicializuje desku a nastaví pozici doprostřed na zadanou stranu.
    - board_move_up
        - Změní pozici desky nahoru.
    - board_move_down
        - Změní pozici desky dolů.
    - board_move_auto
        - Jednoduché AI pro automatické řízení desky.
    - board_draw
        - Vykreslí desku na dané pozici.

- kbhit.c
    - change_mode
        - Nastaví stav terminálu na zápis do terminálu/čtení z termínálu.
    - kbhit
        - Zjistí, zda byla stisknuta klávesnice či nikoliv.

- display_utils.c
    - mem_base_init
        - Inicializuje adresu k přístupu k perifériím.
    - display_unit
        - Inicializuje adresu k přístupu k displeji.
    - clear_buffer
        - Nastaví pole s daty, určenými k vykreslení na displeji, na 0 (=černá barva).
    - clear_display
        - Celý displej nastaví na čenou barvu.
    - draw_pixel
        - Vykreslí pixel dané barvy na zadanou pozici.
    - draw_pixel_big
        - Vykreslí zvětšený pixel zadané barvy na zadanou pozici.
    - char_width
        - Zjistí šířku zadaného znaku podle fontu.
    - draw_char
        - Vykreslí znak dané barvy a fontu na zadanou pozici.
    - draw_word
        - Vykreslí slovo dané barvy a fontu na zadanou pozici.

- led_utils.c
    - end_game_led_line
        - Rozsvítí každou lichou nebo sudou LED, podle modu.
    - life_lose_led_line
        - Rozsvítí celý LED pásek.
    - clear_led_line
        - Zhasne celý LED pásek.
    - end_game_led
        - Rozsvítí obě dvě barevné LED barvou vyhrávajícího hráče.
    - life_lose_led
        - Rozbliká barevnou LED na straně skorujícího hráče jeho barvou.
    - clear_led
        - Zhasne obě dvě barevné LED.

- game_properties.h
    - Obsahuje konstanty potřebné k běhu hry.

- mzapo_parlcd.c
    - Obsahuje funkce potřebné k práci s LCD displejem.

- mzapo_phys.c
    - Obsahuje mapování k fyzické adrese.

- font_prop14x16.c
    - Windows FreeSystem 14x16 Font.

- font_rom8x16.c
    - ROM 8x16 Font bios mode 12.

- mzapo_regs.h
    - Definice MZ_APO registrů.

- serialize_lock.c
    - Slouží k vyřešení kolizí při používání jedné desky více uživateli.


## Kompilace, spuštění

Kompilace přes příkazový řádek pomocí příkazu 'make'. V souboru Makefile na 18. řádku možnost
nastavení IP adresy v případě připojování přes server postel.
Spuštění přes příkaz 'make run'. 
    
