#ifndef __LED_UTILS_H__
#define __LED_UTILS_H__


/* 
 * Function:  end_game_led_line
 * --------------------
 * Blinks with every odd or even diode in LED band
 * 
 * val_line: pointer to led line memory
 * mode: set true if all odd leds should shine
 *      set false if all even leds should shine
 */
void end_game_led_line(unsigned char *mem_base, bool mode);

/* 
 * Function:  life_lose_led_line
 * --------------------
 * Flash full led line
 *
 * val_line: pointer to led line memory
 */
void life_lose_led_line(unsigned char *mem_base);

/* 
 * Function:  clear_led_line
 * --------------------
 * Clear led line so it does not shine
 *
 * val_line: pointer to led line memory
 */
void clear_led_line(unsigned char *mem_base);

/* 
 * Function:  end_game_led
 * --------------------
 * Shine both LED with winning player's color
 * 
 * mem_base: pointer to line memory
 * player: defines which player wins game, shine both LED with player's color
 */
void end_game_led(unsigned char *mem_base, signed char player);

/* 
 * Function:  life_lose_led
 * --------------------
 * Shine both LED with winning player's color
 * 
 * mem_base: pointer to line memory
 * player: defines which player wins round, shine both LED with player's color
 */
void life_lose_led(unsigned char *mem_base, short player);

/* 
 * Function:  clear_led
 * --------------------
 * Flear both led so it does not shine
 *
 * mem_base: pointer to led memory
 */
void clear_led(unsigned char *mem_base);

#endif  // __LED_UTILS_H__