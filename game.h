#ifndef __GAME_H__
#define __GAME_H__

#include "board.h"
#include "ball.h"
#include "game_properties.h"
#include "game_draw.h"

/* 
 * Enum for game mode. 
 */
typedef enum {AUTOPLAYER, SINGLEPLAYER, MULTIPLAYER} mode;

/* 
 * Function:  game_run
 * --------------------
 * Run game with choosen mode.
 *
 * m: game mode
 * parldc_mem_base: pointer to display memory
 */
void game_run(mode m, unsigned char *parlcd_mem_base);

#endif  // __GAME_H__