#ifndef __BALL_H__
#define __BALL_H__

#include "game_properties.h"

struct Ball{
    float velX;
    float velY;
    float posX;
    float posY;
};

/* 
 * Function:  init_ball
 * --------------------
 * Initialize ball and set position to middle.
 *
 * dir: -1 to start moving to left side, 
 *       1 to start moving to left side
 */
struct Ball ball_init(signed char dir);

/* 
 * Function:  move_ball
 * --------------------
 * Update the position of ball by magnitude of vectors x and y. 
 * 
 * b: the ball with which it moves 
 */
void ball_move(struct Ball *b);

/* 
 * Function:  check_bounce
 * -----------------------
 * Check for hitting the top or bottom wall and apply bounce if so. 
 *
 * b: the ball which is checked
 */
void ball_check_bounce(struct Ball *b);

/* 
 * Function:  draw_ball
 * -----------------------
 * Draw ball on his positions.
 *
 * b: pointer to ball to be drawn
 * fb: data buffer given to display
 */
void ball_draw(struct Ball *b, unsigned short *fb);

#endif  // __BALL_H__